/**
 * 全局的socket对象
 */
var websocket = null;

/**
 * 获取后台的socket连接
 * @param moduleStr 传入模块名,如chat，user，essay，stock等
 * @returns
 */
function getSocket(module) {
    var webSocket = null;
    var connectUrl = "";
    var connectPort = "";
    if (module == "stock") {
        connectPort = ":8762";
    }else if (module == "chat") {
        connectPort = ":8080";
    }
    if (window.location.protocol == 'http:') {
        connectUrl = 'ws://' + window.location.host.replace(":8080", "") + connectPort + '/websocket/' + module;
    } else {
        connectUrl = 'wss://' + window.location.host.replace(":8080", "") + connectPort + '/websocket/' + module;
    }

    if ('WebSocket' in window) {
        webSocket = new WebSocket(connectUrl);
    } else if ('MozWebSocket' in window) {
        webSocket = new MozWebSocket(connectUrl);
    } else {
        Console.log('Error: WebSocket is not supported by this browser.');
        return;
    }
    return webSocket;

}