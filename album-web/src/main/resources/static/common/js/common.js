//得到正确的datetime格式的时间
function getCurrentDateTime(datetime) {
    return datetime.substring(0, datetime.length - 2);
}

function setDefaultImageHeadIfError(id) {
    document.getElementById(id).setAttribute("onerror", "javascript:this.src='/common/image/imageHead.jpg';");
}

/**
 * 检测用户登录状态，跳转到指定的路径
 * @param obj 登录成功后，跳转的url
 */
function checkLogin(obj) {
    var url = obj.getAttribute("url");

    $.ajax({
        url: "/user/checkuserlogininfo",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                window.location.href = url;
            } else {
                localStorage.setItem("result", JSON.stringify(result));
                window.location.href = "/common/errorMessage.html"
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}

/**
 * 检查是否登陆
 * @returns {Boolean}
 */
function checkUserLogin() {
    var isSuccess = false;
    $.ajax({
        url: "/user/checkuserlogininfo",
        dataType: "json",
        type: "POST",
        async: false,
        success: function (result) {
            if (result.success) {
                isSuccess = true;
            } else {
                isSuccess = false;
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
    return isSuccess;
}

function checkDevice(obj) {
    var url = obj.getAttribute("url");

    if (document.body.clientWidth < 768) {
        var result = {};
        result.errorMessage = "该设备不支持编辑功能";
        localStorage.setItem("result", JSON.stringify(result));
        window.location.href = "common/errorMessage.html"
    } else {
        window.location.href = url;
    }


}

//新建文章
function newEssay() {
    $.ajax({
        url: "/user/checkuserlogininfo",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                window.location.href = "/essay/essayEdit.html";
            } else {
                alert("请先登录");
                $('#loginModal').modal('show');
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });

}
