$(document).ready(function () {
    //document.getElementById("header").style.display = "none";
});


//注册用户信息
function registerUserInfo() {
    var registerPhone = document.getElementById("registerPhone").value;
    var registerUserName = document.getElementById("registerUserName").value;
    var registerPassword = document.getElementById("registerPassword").value;
    var registerPasswordAgain = document.getElementById("registerPasswordAgain").value;

    var userInfo = {};
    userInfo["telephone"] = registerPhone;
    userInfo["username"] = registerUserName;
    userInfo["password"] = registerPassword;
    userInfo["registerPasswordAgain"] = registerPasswordAgain;
    $.ajax({
        data: userInfo,
        url: "user/userregister",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                alert("注册成功");
                window.location.href = "userLogin.html";
            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}