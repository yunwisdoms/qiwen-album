var editAlbumIndex;
var albumListApp;
$(document).ready(function () {
    albumListApp = new Vue({
        el: '#albumListId',
        data: {
            albumList: []
        },
        created: function () {
            this.initAlbum();
            showStorage();
        },
        methods: {
            initAlbum: function () {
                $.ajax({
                    url: "/album/getalbumlist",
                    dataType: "json",
                    type: "POST",
                    success: function (result) {
                        if (result.success) {
                            var dataArr = [];
                            var data = result.data;
                            var html = "<ul>"
                            for (var i = 0; i < data.length; i++) {
                                var dataMap = {
                                    imageUrl: (data[i].fileBeanList.length <= 1) ? "image/picNone.png" : (data[i].fileBeanList[0].fileurl == null ? data[i].fileBeanList[1].fileurl : data[i].fileBeanList[0].fileurl),
                                    albumId: data[i].albumid,
                                    imageCount: data[i].fileBeanList.length - 1,
                                    albumName: data[i].albumname,
                                    showAlbumUrl: "showAlbum.html?albumId=" + data[i].albumid+"&albumName="+data[i].albumname+
                                            "&albumpower="+data[i].albumpower,
                                    // editAlbumUrl: "editAlbum.html?albumId=" + data[i].albumid+"&albumName="+data[i].albumname+
                                    //         "&albumintro="+data[i].albumintro+"&albumpower="+data[i].albumpower,
                                    albumintro: data[i].albumintro,
                                    isShow: false,
                                    albumpower: data[i].albumpower
                                };
                                dataArr.push(dataMap);
                            }
                            albumListApp.albumList = dataArr;

                        } else {
                            alert("失敗" + result);
                        }
                    },
                    error: function (result) {
                        //alert("系统繁忙!");
                    }
                });
            },
            photoListEnter: function(param) {
                this.albumList[param].isShow = true;
            },
            photoListLeave: function (param) {
                this.albumList[param].isShow = false;
            },
            editAlbumClick: function (album,indexAlbum) {
                editAlbumIndex = layer.open({
                    title:"编辑相册",
                    type: 2,
                    area: ['700px', '450px'],
                    fixed: false, //不固定
                    maxmin: true,
                    btn: ['确定', '取消'],
                    content: "editAlbum.html?albumId=" + album.albumId+"&albumName="+album.albumName+ "&albumintro="+album.albumintro+"&albumpower="+album.albumpower,
                    yes: function(index, layero){

                        var iframeWin = window[layero.find('iframe')[0].name];
                        iframeWin.saveEditAlbumInfo(indexAlbum);
                    },
                    btn2: function(index, layero){
                        layer.close(index);
                    },
                    cancel: function(){
                        //右上角关闭回调

                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            }
        }
    });
    //弹出层
    layui.use('layer', function () {
        var $ = layui.jquery, layer = layui.layer;

        $('.deleteButton').on('click', function () {
            var othis = $(this), method = othis.data('method');
            active[method] ? active[method].call(this, othis) : '';
        });
        //触发事件
        var active = {
            offset: function (othis) {
                var type = othis.data('type'),
                    text = "确认删除吗？",
                    albumId = othis.data('id'),
                    albumIndex = othis.data('index');
                layer.open({
                    type: 1, offset: type,
                    id: 'layerDemo' + type,
                    content: '<div style="padding: 20px 100px;">' + text + '</div>',
                    btn: ['确认', '取消'],
                    btnAlign: 'c',
                    shade: 0,
                    yes: function () {
                        layer.closeAll();
                        deleteAlbum(albumId, albumIndex);
                    }
                });
            },
            confirmTrans: function () {
                layer.msg('已删除', {
                    time: 2000
                });
            }
        };
    });

});
function showStorage() {
    $.ajax({
        url: "/filetransfer/getstorage",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                $("#storageid").text((result.data.storagesize/(1024 * 1024)).toFixed(2));
            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}
function deleteAlbum(albumId, albumIndex) {
    var albumData = {
        albumid: albumId
    };
    $.ajax({
        data: albumData,
        url: "/album/deletealbum",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                albumListApp.albumList.splice(albumIndex,1);
            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}