var albumId;
var albuminfo = {};
$(document).ready(function () {
    var thisURL = document.URL;
    //url解码
    var getval = decodeURI(thisURL).split('?')[1].split("&");
    //url参数转换为键值对
    for(var i = 0; i < getval.length; i++) {
        var albuminfokey = getval[i].split("=")[0];
        var albuminfovalue = getval[i].split("=")[1];
        albuminfo[albuminfokey] = albuminfovalue;
    }

    // 表单初始赋值
    $("#albumNameId").val(albuminfo.albumName);
    $("#albumIntrolId").val(albuminfo.albumintro);
    var _albumpower = $("input[name='albumpower']");
    for(i = 0; i < _albumpower.length; i++) {
        if(_albumpower[i].value == albuminfo.albumpower) {
            _albumpower[i].checked = true;
        }else {
            _albumpower[i].checked = false;
        }
    }
});

//保存修改信息
function saveEditAlbumInfo(param) {
    var albumname = document.getElementById("albumNameId").value;
    var albumIntro = $("#albumIntrolId")[0].value;
    var albumpower = $("input[name='albumpower']:checked").val();
    var albumData = {
        albumid: albuminfo.albumId,
        albumname: albumname,
        albumintro: albumIntro,
        albumpower: albumpower
    };
    $.ajax({
        data: albumData,
        url: "/album/updatealbum",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {

                parent.layer.close(parent.editAlbumIndex);
                parent.albumListApp.albumList[param].albumName = albumData.albumname;
                parent.albumListApp.albumList[param].albumintro = albumData.albumintro;
                parent.albumListApp.albumList[param].albumpower = albumData.albumpower;
                // parent.$(".photoName")[param].innerText = albumData.albumname;
                parent.alertMsg(result.data);
            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}

// 重置编辑信息
function resetAlbumInfo() {
    // $("#albumNameId").val("");
    // $("#albumIntrolId").val("");
    // $("input[name='albumpower']:checked").val(albuminfo.albumpower);
    parent.layer.close(parent.editAlbumIndex);
}