package com.qiwenshare.web.domain;

import javax.persistence.*;

/**
 * 用户关注类
 *
 * @author ma116
 */
@Table(name = "userattention")
@Entity
public class UserAttentionBean {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userAttentionId;
    @Column
    private long userId;
    @Column
    private long otherUserId;
    @Transient
    private UserBean attentionedUser;

    public long getUserAttentionId() {
        return userAttentionId;
    }

    public void setUserAttentionId(long userAttentionId) {
        this.userAttentionId = userAttentionId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getOtherUserId() {
        return otherUserId;
    }

    public void setOtherUserId(long otherUserId) {
        this.otherUserId = otherUserId;
    }

    public UserBean getAttentionedUser() {
        return attentionedUser;
    }

    public void setAttentionedUser(UserBean attentionedUser) {
        this.attentionedUser = attentionedUser;
    }
}
