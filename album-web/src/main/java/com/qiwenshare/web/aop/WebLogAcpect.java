package com.qiwenshare.web.aop;

import com.qiwenshare.common.cbb.RestResult;
import com.qiwenshare.web.anno.MyLog;
import com.qiwenshare.web.api.IOperationLogService;
import com.qiwenshare.web.cbb.OperationLogUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 操作日志切面
 */
@Aspect
@Component
public class WebLogAcpect {
    @Resource
    IOperationLogService operationLogService;
    private String operation = "";
    private String module = "";
    private HttpServletRequest request;

    private Logger logger = LoggerFactory.getLogger(WebLogAcpect.class);

    /**
     * 定义切入点，切入点为com.example.aop下的所有函数
     */
    @Pointcut("@annotation(com.qiwenshare.web.anno.MyLog)")
    public void webLog() {
    }

    /**
     * 前置通知：在连接点之前执行的通知
     *
     * @param joinPoint 切入点
     */
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint){
        //从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        //获取切入点所在的方法
        Method method = signature.getMethod();
        //获取操作
        MyLog myLog = method.getAnnotation(MyLog.class);

        if (myLog != null) {
            operation = myLog.operation();
            module = myLog.module();
        }

        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        request = attributes.getRequest();


    }

    @AfterReturning(returning = "ret", pointcut = "webLog()")
    public void doAfterReturning(Object ret) throws Throwable {

        if (ret instanceof RestResult){
            boolean isSuccess = ((RestResult) ret).isSuccess();
            String errorMessage = ((RestResult) ret).getErrorMessage();

            if (isSuccess){
                operationLogService.insertOperationLog(
                        OperationLogUtil.getOperationLogObj(request, "成功", module, operation, "操作成功"));
            }else{
                operationLogService.insertOperationLog(
                        OperationLogUtil.getOperationLogObj(request, "失败", module, operation, errorMessage));
            }
        }


    }
}