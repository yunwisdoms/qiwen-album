package com.qiwenshare.web.mapper;

import com.qiwenshare.web.domain.UserAttentionBean;

import java.util.List;


public interface UserAttentionMapper {
    int insertAttentionUser(UserAttentionBean userAttentionBean);

    List<UserAttentionBean> selectUserAttentionByUserId(UserAttentionBean userAttentionBean);

    int deleteAttentionUser(UserAttentionBean userAttentionBean);

    int selectAttentionCountByUserId(int userId);

    List<UserAttentionBean> selectAttentionList(int userId);

    int selectFanCountByUserId(int userId);
}
