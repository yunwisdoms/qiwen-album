package com.qiwenshare.web.mapper;

import com.qiwenshare.web.domain.RemarkBean;
import com.qiwenshare.web.domain.ReplyBean;
import com.qiwenshare.web.domain.UserBean;

import java.util.List;

public interface RemarkMapper {
    boolean addRemark(RemarkBean remarkBean);

    boolean addReply(ReplyBean replyBean);

    List<RemarkBean> getRemarkByTargetId(RemarkBean remarkBean);

    List<ReplyBean> getReplyByRemarkId(long remarkId);

    boolean delRemarkByRemarkId(long remarkId);

    boolean delReplyByRemarkId(long remarkId);

    void delRemarkByTargetId(long targetid);

    int selectRemarkCountByUser(UserBean userBean);
}
