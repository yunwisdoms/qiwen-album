package com.qiwenshare.web.mapper;

import com.qiwenshare.web.domain.*;
import com.qiwenshare.web.domain.StorageBean;
import com.qiwenshare.web.domain.UserImageBean;

import java.util.List;

public interface FiletransferMapper {

    /**
     * 插入用户头像
     *
     * @param userImageBean
     */
    void insertUserImage(UserImageBean userImageBean);


    List<UserImageBean> selectUserImage(long userId);

    void deleteUserImageByIds(List<Integer> imageidList);

    StorageBean selectStorageBean(StorageBean storageBean);

    void insertStorageBean(StorageBean storageBean);

    void updateStorageBean(StorageBean storageBean);

    StorageBean selectStorageByUser(StorageBean storageBean);
}
