package com.qiwenshare.web.service;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.qiwenshare.common.cbb.DateUtil;
import com.qiwenshare.common.cbb.RestResult;
import com.qiwenshare.common.cbb.Uploader;
import com.qiwenshare.common.domain.UploadFile;
import com.qiwenshare.web.api.IFiletransferService;

import com.qiwenshare.web.domain.*;
import com.qiwenshare.web.mapper.AlbumMapper;
import com.qiwenshare.web.mapper.FileMapper;
import com.qiwenshare.web.mapper.FiletransferMapper;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;


@Service
public class FiletransferService implements IFiletransferService {

    @Resource
    FiletransferMapper filetransferMapper;
    @Resource
    FileMapper fileMapper;
    @Resource
    AlbumMapper albumMapper;

//    @Override
//    public ImageBean selectImageBean(ImageBean imageBean){
//        return fileMapper.selectFileById().select.selectImageBean(imageBean);
//    }

//    @Override
//    public void deleteUserImageById(UserImageBean userImageBean) {
//        filetransferMapper.deleteUserImageById(userImageBean);
//
//    }

//    @Override
//    public void deleteImageById(ImageBean imageBean) {
//        filetransferMapper.deleteImageById(imageBean);
//
//    }

    /**
     * 添加用户头像
     */
    @Override
    public RestResult<String> insertUserImage(UserImageBean userImageBean) {
        RestResult<String> restResult = new RestResult<String>();
        filetransferMapper.insertUserImage(userImageBean);

        restResult.setSuccess(true);
        return restResult;
    }


    /**
     * 上传用户头像
     */
    @Override
    public RestResult<String> uploadUserImage(HttpServletRequest request) {
        RestResult<String> restResult = new RestResult<String>();
        UserBean sessionUserBean = (UserBean) SecurityUtils.getSubject().getPrincipal();
        UserImageBean userImageBean = new UserImageBean();
        UserBean userBean = null;
        //判断用户是否登陆
        if (sessionUserBean == null) {
            restResult.setErrorCode("用户未登陆");
            restResult.setSuccess(false);
        } else {
            Uploader uploader = null;
            uploader = new Uploader(request);
            List<UploadFile> uploadFile = uploader.upload();

            String imageurl = uploadFile.get(0).getUrl();
            userBean = sessionUserBean;
            userImageBean.setUserid(userBean.getUserId());
            userImageBean.setImageurl(imageurl);
            userImageBean.setUploadtime(DateUtil.getCurrentTime());
            insertUserImage(userImageBean);
            restResult.setData("filetransfer/" + imageurl);
            restResult.setSuccess(true);
        }

        return restResult;
    }

//    @Override
//    public List<UserImageBean> selectUserImageByUrl(String url) {
//        List<UserImageBean> result = filetransferMapper.selectUserImageByUrl(url);
//        return result;
//    }
//
//    @Override
//    public List<ImageBean> selectUserImageByIds(List<Integer> imageidList) {
//        List<ImageBean> result = filetransferMapper.selectUserImageByIds(imageidList);
//        return result;
//    }



//    @Override
//    public void insertImage(ImageBean imageBean) {
//        filetransferMapper.insertImage(imageBean);
//
//    }

//    public void insertFile(FileBean fileBean){
//        filetransferMapper.insertFile(fileBean);
//    }

    @Override
    public void uploadImage(HttpServletRequest request, AlbumBean albumBean) {
//        ImageBean imageBean = new ImageBean();
        FileBean fileBean = new FileBean();
        albumBean = albumMapper.selectAlbum(albumBean);
        UserBean sessionUserBean = (UserBean) SecurityUtils.getSubject().getPrincipal();
//        imageBean.setAlbumid(albumBean.getAlbumid());
        Uploader uploader = new Uploader(request);
        List<UploadFile> uploadFileList = uploader.upload();
        for (int i = 0; i < uploadFileList.size(); i++){
            UploadFile uploadFile = uploadFileList.get(i);
            if (uploadFile.getSuccess() == 1){
//                imageBean.setImageurl(uploadFile.getUrl());
//                imageBean.setImagesize(uploadFile.getFileSize());
//                imageBean.setImagename(uploadFile.getFileName());
//                imageBean.setExtendname(uploadFile.getFileType());
//                imageBean.setTimestampname(uploadFile.getTimeStampName());
//                imageBean.setUploadtime(DateUtil.getCurrentTime());
//
//                insertImage(imageBean);

                fileBean.setAlbumid(albumBean.getAlbumid());
                fileBean.setUserid(sessionUserBean.getUserId());

                fileBean.setFileurl(uploadFile.getUrl());
                fileBean.setFilesize(uploadFile.getFileSize());
                fileBean.setFilename(uploadFile.getFileName());
                fileBean.setExtendname(uploadFile.getFileType());
                fileBean.setTimestampname(uploadFile.getTimeStampName());
                fileBean.setUploadtime(DateUtil.getCurrentTime());
                fileBean.setFilepath("/我的相册/" + albumBean.getAlbumname()+ "/");
                fileMapper.insertFile(fileBean);
            }


            synchronized (FiletransferService.class) {


                long sessionUserId = sessionUserBean.getUserId();
                StorageBean storageBean = selectStorageBean(new StorageBean(sessionUserId));
                if (storageBean == null) {
                    StorageBean storage = new StorageBean(sessionUserId);
                    storage.setStoragesize(fileBean.getFilesize());
                    insertStorageBean(storage);
                } else {
                    storageBean.setStoragesize(storageBean.getStoragesize() + uploadFile.getFileSize());
                    updateStorageBean(storageBean);
                }
            }

        }

    }

    @Override
    public void uploadFile(HttpServletRequest request, FileBean fileBean) {
        Uploader uploader = new Uploader(request);
        List<UploadFile> uploadFileList = uploader.upload();
        for (int i = 0; i < uploadFileList.size(); i++){
            UploadFile uploadFile = uploadFileList.get(i);
            if (uploadFile.getSuccess() == 1){
                fileBean.setFileurl(uploadFile.getUrl());
                fileBean.setFilesize(uploadFile.getFileSize());
                fileBean.setFilename(uploadFile.getFileName());
                fileBean.setExtendname(uploadFile.getFileType());
                fileBean.setTimestampname(uploadFile.getTimeStampName());
                fileBean.setUploadtime(DateUtil.getCurrentTime());

                fileMapper.insertFile(fileBean);
            }


            synchronized (FiletransferService.class) {
                UserBean sessionUserBean = (UserBean) SecurityUtils.getSubject().getPrincipal();

                long sessionUserId = sessionUserBean.getUserId();
                StorageBean storageBean = selectStorageBean(new StorageBean(sessionUserId));
                if (storageBean == null) {
                    StorageBean storage = new StorageBean(sessionUserId);
                    storage.setStoragesize(fileBean.getFilesize());
                    insertStorageBean(storage);
                } else {
                    storageBean.setStoragesize(storageBean.getStoragesize() + uploadFile.getFileSize());
                    updateStorageBean(storageBean);
                }
            }

        }
    }

    @Override
    public void deleteImageByAlbum(AlbumBean albumBean) {
        fileMapper.deleteFileByAlbum(albumBean);

    }

    @Override
    public StorageBean selectStorageBean(StorageBean storageBean) {
        return filetransferMapper.selectStorageBean(storageBean);
    }

    @Override
    public void insertStorageBean(StorageBean storageBean) {
        filetransferMapper.insertStorageBean(storageBean);
    }

    @Override
    public void updateStorageBean(StorageBean storageBean) {
        filetransferMapper.updateStorageBean(storageBean);
    }

    @Override
    public StorageBean selectStorageByUser(StorageBean storageBean) {
        return filetransferMapper.selectStorageByUser(storageBean);
    }
}
