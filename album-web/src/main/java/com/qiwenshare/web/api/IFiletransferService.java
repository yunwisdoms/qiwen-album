package com.qiwenshare.web.api;

import com.qiwenshare.common.cbb.RestResult;
import com.qiwenshare.web.domain.AlbumBean;
import com.qiwenshare.web.domain.FileBean;
import com.qiwenshare.web.domain.StorageBean;
import com.qiwenshare.web.domain.UserImageBean;

import javax.servlet.http.HttpServletRequest;

public interface IFiletransferService {


    /**
     * 用户头像插入数据库
     *
     * @param userImageBean 用户头像信息
     * @return 插入结果
     */
    RestResult<String> insertUserImage(UserImageBean userImageBean);

    /**
     * 上传头像
     *
     * @param request 请求
     * @return 结果
     */
    RestResult<String> uploadUserImage(HttpServletRequest request);

    /**
     * 上传图片
     * @param request 请求
     * @param albumBean 相册信息
     */
    void uploadImage(HttpServletRequest request, AlbumBean albumBean);

    /**
     * 上传文件
     * @param request 请求
     * @param fileBean 文件信息
     */
    void uploadFile(HttpServletRequest request, FileBean fileBean);

    /**
     * 删除相册中的所有图片
     *
     * @param albumBean 相册信息
     */
    void deleteImageByAlbum(AlbumBean albumBean);

    StorageBean selectStorageBean(StorageBean storageBean);

    void insertStorageBean(StorageBean storageBean);

    void updateStorageBean(StorageBean storageBean);

    StorageBean selectStorageByUser(StorageBean storageBean);
}
