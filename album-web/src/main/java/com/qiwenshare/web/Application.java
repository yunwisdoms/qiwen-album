package com.qiwenshare.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication//(exclude={DataSourceAutoConfiguration.class})
@EnableCaching //开启ehcache缓存
@MapperScan("com.qiwenshare.web.mapper")
@EnableScheduling  //启用定时任务
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        //SocketManager.getInstance();
    }


    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
